#!/bin/bash
lookfor='"(.*)"'
#lookforclass='"(.*)", "(.*)"'
#lookforclass='"(*?)\", \"(*)"\$'
#lookforclass='(.*)\ \=\ \"(.*)\",\ \"(.*?)\"'
lookforclass='"(.*)", "(.*?)"'
verbose=0
for arg; do
    case $arg in
        d)
            sleep 1
            ;;
        v)
            verbose=1
            ;;
        vv)
            verbose=2
            ;;
    esac        
done

wnd_focus=$(xdotool getwindowfocus)
#wnd_name=$(xprop -id $wnd_focus WM_NAME| awk '{printf $NF}' | sed 's/"//g')
wnd_name=$(xprop -id $wnd_focus WM_NAME)
#wnd_class=$(xprop -id $wnd_focus WM_CLASS | awk '{printf $NF}')
wnd_class=$(xprop -id $wnd_focus WM_CLASS)
function ShowNameAndClass () {
    case $1 in
        wmClass)
            echo "class:"
            echo $wnd_class 
            ;&
        wmName)
            echo "name and class:"
            echo $wnd_name 
            ;;
        esac
}
function SaveNameAndClass () {
    case $1 in
        wmClass)
            echo $wnd_class > ~/winClass.txt
            ;&
        wmName)
            echo $wnd_name > ~/winName.txt
            ;;
        esac
}
# clear out the special regex chars for name then check length
# if length too long cut the last couple of words that for the noted cases show the name of the program or website running
# concatenate the strings then replace spaces or /'s with _
function CleanNameAndCheckLength () {
            wnd_name=$(echo $wnd_name | sed -r 's/[]:",#?![]//g')
            [[ ${#wnd_name}  -gt 223 ]] && { # 223 = 255 max char filename - 28 for date - 4 .png
                wnd_name_last_word=$(echo "${wnd_name}" | awk '{print $NF}')
            case $wnd_name_last_word in
                Firefox)
                    wnd_name_last_word=$(echo "${wnd_name}" | awk '{print $(NF-3)}')
                    ;;
                Waterfox)
                    wnd_name_last_word=$(echo "${wnd_name}" | awk '{print $(NF-2)}')
                    ;;
                VIM)
                    wnd_name_last_word=$(echo "${wnd_name}" | awk '{print $NF}')
                    ;;
            esac
                echo ${#wnd_name_last_word}
                cut_loc=$(( 222 - ${#wnd_name_last_word} ))
                echo $cut_loc
                #wnd_name_cut=$($wnd_name:0:(222-${#wnd_name_last_word}))) # 222 = 223 - 1 for - to signify cut
                #wnd_name_cut=${$wnd_name:$cut_size:${#wnd_name}} # the cut section
                #wnd_name_cut=${$wnd_name:$cut_size} # the cut section
                wnd_name_keep=${wnd_name:0:$cut_loc} # the kept section
                wnd_name="${wnd_name_keep}-${wnd_name_last_word}"
            }
            wnd_name=$(echo $wnd_name | sed -r 's/[ /]/_/g')
}

if [[ "${wnd_class}" =~ $lookforclass  ]]; then
            wnd_class=${BASH_REMATCH[1]}
        if [[ "${wnd_name}" =~ $lookfor ]]; then
            wnd_name=${BASH_REMATCH[1]}
            #wnd_name="${wnd_name/\"/}"
            #wnd_name=$(echo $wnd_name | sed -r 's/ /_/g; s/["#?!]//g')
            #wnd_name=$(echo $wnd_name | sed -r 's/[]",()#?![]//g')
            CleanNameAndCheckLength
            [[ $verbose  -gt 0 ]] && (ShowNameAndClass wmClass)
            [[ $verbose  -gt 1 ]] && (SaveNameAndClass wmClass)

            wnd_class=$(echo $wnd_class | sed -r 's/[]:",#?![]//g; s/[ /]/_/g') # clear the special chars from the class name
                                                                                # also replace the spaces and /'s with _
            mkdir ~/Screenshots/"${wnd_class}"
            scrot '%Y-%m-%d-%H%M%S_$wx$h_'"${wnd_name}"'.png' -u -e 'mv \"$f\" ~/Screenshots/'"${wnd_class}"'/'
            #scrot '%Y-%m-%d-%H%M%S_$wx$h_'"${wnd_name}"'.png' -u -e 'mv $f ~/Screenshots/$wnd_class/'
            #scrot '%Y-%m-%d-%H%M%S_$wx$h_'"${wnd_name}"'.png' -u -e 'mv %Y-%m-%d-%H%M%S_$wx$h_'"${wnd_name}"'.png ~/Screenshots/'"${wnd_class}"'/'
        fi
    else
        if [[ "${wnd_name}" =~ $lookfor ]]; then
            wnd_name=${BASH_REMATCH[1]}
            CleanNameAndCheckLength
            [[ $verbose  -gt 0 ]] && ShowNameAndClass wmName
            [[ $verbose  -gt 1 ]] && SaveNameAndClass wmName
            mkdir ~/Screenshots/"${wnd_name}"
            scrot '%Y-%m-%d-%H%M%S_$wx$h_'"${wnd_name}"'.png' -u -e 'mv \"$f\" ~/Screenshots/'"${wnd_name}"'/'
        fi
fi

